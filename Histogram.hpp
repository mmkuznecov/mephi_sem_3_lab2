#pragma once

#include "Person.hpp"
#include "Sequence.hpp"
#include "Dictionary.hpp"
#include "CompareFuncs.hpp"

using namespace std;

class Histogram{

    private:

        Dictionary<int, int>* dict;


        int (Person::*getParam)();

    public:

        Histogram(ArraySequence<Person> seq, int (Person::*getParam)(), int n)
        {

            int min = (seq.Get(0).*getParam)();
            int max = (seq.Get(0).*getParam)();

            for (int i = 1; i < seq.GetSize(); i++){
                if ((seq.Get(i).*getParam)() < min)
                    min = (seq.Get(i).*getParam)();
                
                if ((seq.Get(i).*getParam)() > max)
                    max = (seq.Get(i).*getParam)();
            }
            int delta = (max - min) / n;

            while (!delta){
                n--;
                delta = (max - min) / n;
            }
            
            dict = new Dictionary<int, int>(min, 0, compareT);
            
            for (int i = 1; i < n; i++)
                dict->Add((min + delta * i), 0);

            int j = 0;
            int num = 0;
            
            for (int i = 0; i < seq.GetSize(); i++)
            {
                j = (((seq.Get(i).*getParam)() - min) / delta);
                if (j == n) j--;
                num = dict->Get((min + j * delta));
                dict->ChangeElem((min + j * delta), num + 1);
            }
            this->getParam = getParam;
        }

        ~Histogram() = default;

        Sequence<PairKE<int, int>>* print_hist()
        {
            Sequence<PairKE<int, int>>* seq = dict->Get_Array();
            return seq;
        }
};

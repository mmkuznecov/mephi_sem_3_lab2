#include <iostream>
#include <string>
#include <chrono>
#include "Person.hpp"
#include "Histogram.hpp"
#include "CompareFuncs.hpp"

using namespace std;


string hist_params[2] = {"1--Age", "2--Social rating"};
string fill_ch[2] = {"1--Random", "2--Custom"};

string fill_descr = "Choose way to generate people";
string hist_descr = "Choose hist parameter";

string fnames[5] = {"A", "B", "C", "D", "E"};
string mnames[5] = {"A", "B", "C", "D", "E"};
string lnames[5] = {"A", "B", "C", "D", "E"};


Person GetPerson(int choice){

    string fname, mnane, lname;
    int age, social_rating;

    if(choice-1){

        cout << "Input First Name: ";
        cin >> fname;
        cout << "Input Middle Name: ";
        cin >> mnane;
        cout << "Input Last Name: ";
        cin >> lname;
        cout << "Input Social Rating: ";
        cin >> social_rating;
        cout << "Input Age: ";
        cin >> age;

    }
    else{

        fname = fnames[rand() % 5];
        mnane = mnames[rand() % 5];
        lname = lnames[rand() % 5];
        age = rand() % 100;
        social_rating = rand() % 1000;

        cout << Person(fname, mnane, lname, social_rating, age) << endl;
    }

    return Person(fname, mnane, lname, social_rating, age);

}



int GetOption(string MSGS[], int len, string description){

    cout << "=======================" << endl;

    cout << description << endl;

    for (int i = 0; i < len; i++){
        cout << MSGS[i] << endl;
    }

    cout << "=======================" << endl;

    int choice;

    cout << "Enter your choice: ";
    cin >> choice;

    return choice;
}

void RunHist(){


    int choice, count, number, param = 0;

    ArraySequence<Person> *seq = new ArraySequence<Person>();
    int (Person::*getParam)() = NULL;
    Histogram *hist;
    
    choice = GetOption(fill_ch, 2, fill_descr);

    cout << "Number of people: " << endl;
    cin >> count;

    for (int i = 0; i < count; i++){
        seq->Append(GetPerson(choice));
    }


    param = GetOption(hist_params, 2, hist_descr);


    if (param == 1){getParam = &Person::GetAge;}
    else{getParam = &Person::GetSocialRating;}

    cout << "Subsets number: " << endl;
    cin >> number;

    hist = new Histogram(*seq, getParam, number);
    cout << "The histogram:" << endl;

    Sequence<PairKE<int, int>>* pairs = hist->print_hist();

    for(int i = 0; i < pairs->GetSize(); i += 1)
    {
        cout << pairs->Get(i) << endl;
    }

}


void Menu(){

    cout << "HISTOGRAM" << endl;

    RunHist();

}
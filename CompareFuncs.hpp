#pragma once

#include "Sequence.hpp"
#include "Dictionary.hpp"

using namespace std;

template <class T>
bool compareT(T a, T b){return a <= b;}

bool compare_string_int(PairKE<string, int> a, PairKE<string, int> b) {return (a.GetKey() <= b.GetKey());}
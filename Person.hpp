#include <string>

#pragma once

using namespace std;

class Person{

    private:

        string m_fname;
        string m_mname;
        string m_lname;
        int m_age;
        int m_social_rating;

    public:


        Person(string first_name = "", string middle_name = "", string last_name = "", int social_rating = -1, int age = -1)
            : m_fname{first_name},
            m_mname{middle_name},
            m_lname{last_name},
            m_social_rating{social_rating},
            m_age{age}
        {}

        ~Person() = default;

        string GetFirstname(){return this->m_fname;}
        string GetMiddleName(){return this->m_mname;}
        string GetLastname(){return this->m_fname;}
        int GetSocialRating(){return this->m_social_rating;}
        int GetAge(){return this->m_age;}

        string GetFullname(){return this->m_fname + " " + this->m_mname + " " + this->m_lname;}


        bool operator==(const Person& second){
            return (this->m_fname == second.m_fname && this->m_lname == second.m_lname && this->m_mname == second.m_mname &&
            this->m_age == second.m_age && this->m_social_rating == second.m_social_rating);
        }

    };

std::ostream& operator<< (std::ostream &out, Person person){
    return out << "{{" << person.GetFullname() << ";" << person.GetAge() << ";" << person.GetSocialRating() << "}}";
}

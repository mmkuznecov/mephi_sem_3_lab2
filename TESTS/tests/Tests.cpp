#include "gtest/gtest.h"
#include "../../ListSequence.hpp"
#include "../../ArraySequence.hpp"
#include "../../CompareFuncs.hpp"
#include "../../Histogram.hpp"



TEST(Dictionary, first)
{
    Dictionary<string , int>* dict;
    dict = new Dictionary<string , int>(compare_string_int);

    dict->Add("Ich", 1);
    dict->Add("bin", 2);
    dict->Add("Hans", 3);


    ASSERT_TRUE(dict->ContainsKey("Ich"));
    ASSERT_TRUE(dict->ContainsKey("bin"));
    ASSERT_TRUE(dict->ContainsKey("Hans"));


    ASSERT_FALSE(dict->ContainsKey("Untermensch"));


    ASSERT_EQ(3, dict->Get("Hans"));
    ASSERT_EQ(1, dict->Get("Ich"));
    ASSERT_EQ(2, dict->Get("bin"));



    ASSERT_EQ(3, dict->GetCount());

}

TEST(Dictionary, second)
{
    Dictionary<string , int>* dict1;

    dict1 = new Dictionary<string , int>(compare_string_int);

    dict1->Add("Ich", 1);
    dict1->Add("bin", 2);
    dict1->Add("Hans", 3);


    dict1->ChangeElem("Hans", 5);
    ASSERT_EQ(5, dict1->Get("Hans"));

    dict1->Remove("bin");
    ASSERT_TRUE(dict1->ContainsKey("Ich"));
    ASSERT_FALSE(dict1->ContainsKey("bin"));

    ASSERT_TRUE(dict1->ContainsKey("Hans"));
    ASSERT_EQ(2, dict1->GetCount());
}

TEST(Histogram, first)
{
    ArraySequence<Person> *seq = new ArraySequence<Person>();
    seq->Append(Person("A", "A", "A", 1488, 80));
    seq->Append(Person("B", "B", "B", 222, 90));
    seq->Append(Person("C", "C", "C", 69, 80));
    seq->Append(Person("D", "D", "D", 24, 90));

    int (Person::*getParam)() = NULL;
    getParam = &Person::GetAge;

    Histogram *hist;
    hist = new Histogram(*seq, getParam, 2);

    Sequence<PairKE<int, int>>* pairs = hist->print_hist();


    ASSERT_EQ(80, pairs->Get(0).GetKey());
    ASSERT_EQ(85, pairs->Get(1).GetKey());


    ASSERT_EQ(2, pairs->Get(0).GetElem());
    ASSERT_EQ(2, pairs->Get(1).GetElem());
}